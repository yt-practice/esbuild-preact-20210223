const { h, Fragment } = require('preact')

const React = Object.freeze({ createElement: h, Fragment })
Object.defineProperties(global, { React: { get: () => React } })
