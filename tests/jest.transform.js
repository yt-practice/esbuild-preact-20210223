// Inspired by https://github.com/aelbore/esbuild-jest#readme

const esbuild = require('esbuild')
const pkg = require('../package.json')

const external = [
	...Object.keys(pkg.dependencies ?? {}),
	...Object.keys(pkg.devDependencies ?? {}),
	...Object.keys(pkg.peerDependencies ?? {}),
	...Object.keys(pkg.optionalDependencies ?? {}),
]

module.exports = {
	getCacheKey() {
		// Forces to ignore Jest cache
		return Math.random().toString()
	},
	process(content, filename) {
		const { outputFiles } = esbuild.buildSync({
			target: 'node14',
			platform: 'node',
			bundle: true,
			sourcemap: 'inline',
			loader: {
				// '.js': 'jsx',
				'.css': 'text',
			},
			entryPoints: [filename],
			external,
			write: false,
		})
		if (1 !== outputFiles.length) throw new Error('wtf')
		return outputFiles[0].text
	},
}
