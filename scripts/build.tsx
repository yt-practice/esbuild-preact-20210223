import path from 'path'
import { promises as fs } from 'fs'
import * as esbuild from 'esbuild'
import render from 'preact-render-to-string'

const isDev = 'development' === process.env.NODE_ENV

const main = async () => {
	if (!isDev) {
		const htmlpath = path.join(__dirname, '../dist/index.html')
		const html = await fs.readFile(htmlpath, 'utf-8')
		const { App } = await import('~/app')
		const rendered = await render(<App />)
		const newhtml = html.replace(/(<body[^>]*>)/iu, '$1' + rendered)
		await fs.writeFile(htmlpath, newhtml)
	}
	await esbuild.build({
		entryPoints: ['src/index.tsx'],
		bundle: true,
		outdir: 'dist',
		minify: !isDev,
		sourcemap: isDev,
		platform: 'browser',
		define: { 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV) },
		tsconfig: 'tsconfig.json',
		inject: ['src/_import-preact.ts'],
		jsxFactory: 'h',
		jsxFragment: 'Fragment',
		watch: isDev,
	})
}

main().catch(x => {
	console.error(x)
	process.exit(1)
})
