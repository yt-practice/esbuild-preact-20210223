import { hydrate } from 'preact'
import { App } from '~/app'

export const main = async () => {
	hydrate(<App />, document.body)
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
})
