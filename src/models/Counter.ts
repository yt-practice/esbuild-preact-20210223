export class Counter {
	private constructor(
		private current: number,
		private readonly key: string,
		private readonly store: LocalForage,
	) {}
	async up() {
		this.current++
		await this.store.setItem(this.key, { current: this.current })
	}
	async reset(current = 0) {
		this.current = current
		await this.store.setItem(this.key, { current: this.current })
	}
	get value() {
		return this.current
	}
	static async create(current: number, key: string, store: LocalForage) {
		const c = await store.getItem<{ current: number }>(key)
		return new Counter(c?.current ?? current, key, store)
	}
}
