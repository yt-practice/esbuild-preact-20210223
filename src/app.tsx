import { useEffect, useMemo, useState } from 'preact/hooks'
import { getStore } from './lib/store'
import { Counter } from '~/models/Counter'

export const App = () => {
	const [cnt, setcnt] = useState<Counter | null>(null)
	useEffect(() => {
		getStore().then(async store => {
			const cnt = await Counter.create(0, 'mykey', store)
			setcnt(cnt)
		})
	}, [setcnt])
	if (cnt) return <View cnt={cnt} />
	return (
		<>
			<button name="up">increment</button>
			<button name="reset">reset</button>
			<output>.</output>
		</>
	)
}

const View = (props: { cnt: Counter }) => {
	const { value, onUpClick, onResetClick } = useStore(props.cnt)
	return (
		<>
			<button name="up" onClick={onUpClick}>
				increment
			</button>
			<button name="reset" onClick={onResetClick}>
				reset
			</button>
			<output>{value}</output>
		</>
	)
}

const useStore = (cnt: Counter) => {
	const [value, setvalue] = useState(cnt.value)
	const fns = useMemo(() => {
		const onUpClick = async () => {
			await cnt.up()
			setvalue(cnt.value)
		}
		const onResetClick = async () => {
			await cnt.reset()
			setvalue(cnt.value)
		}
		return { onUpClick, onResetClick }
	}, [setvalue, cnt])
	return { ...fns, value }
}
