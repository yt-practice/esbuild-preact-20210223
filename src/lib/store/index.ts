import localforage from 'localforage'

let st: Promise<LocalForage> | undefined
export const getStore = async () => {
	if (st) return st
	const store = localforage.createInstance({
		driver: localforage.INDEXEDDB,
		name: 'indexeddb_test_app',
		version: 1,
		storeName: 'indexeddb_test_app_store',
	})
	st = Promise.resolve(store)
	return st
}
