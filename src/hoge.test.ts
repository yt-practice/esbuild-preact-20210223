import { h } from 'preact'
import { render } from 'preact-render-to-string'
import { Hoge } from './hoge'

describe('test', () => {
	it('main', () => {
		expect(render(h(Hoge, null))).toBe('<div>hoge</div>')
	})
})
